import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import static java.lang.Thread.sleep;


/**
 * Created by Selinko on 8/1/2018.
 */
public class test {

    public static WebDriver driver;

    @FindBy(css = "#next-button-tracking")
    private WebElement NEXT_BTN;

    @FindBy(css = "a[href='http://www.directexpose.com/beauty-women-world/1/']")
    private WebElement SLIDE_SHOW;

    @FindBy(css = "#mvp-content-main img")
    private WebElement IMAGE;

    @FindBy(css = "#mvp-content-main h2")
    private WebElement H2;

    @FindBy(css = ".navigation-post-item.right")
    private WebElement NEXT_BTN_ON_IMG;

    @FindBy(css = "#mvp-post-head h1")
    private WebElement H1;

    @FindBy(css = "#taboola-last-page-alternating-thumbnails")
    private WebElement LAST_PAGE_INDICATOR;

    @BeforeTest
    public void beforeTest() {
        DesiredCapabilities caps = new DesiredCapabilities();
        System.setProperty("webdriver.chrome.driver", "drivers/chromedriver.exe");
        System.setProperty("browserWidth", "1920");
        driver = new ChromeDriver(caps);
        driver.get("http://www.directexpose.com/beauty-women-world/?utm_content=newnext&utm_source=talas&utm_medium=d33");


    }

    @org.testng.annotations.Test
    public void test() {

        PageFactory.initElements(driver, this);
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        WebDriverWait wait = new WebDriverWait(driver, 40);
        wait.until(ExpectedConditions.visibilityOf(H1));
        Assert.assertEquals(H1.getText(), "A Thousand Shades of Beauty Across the Globe");
        wait.until(ExpectedConditions.visibilityOf(SLIDE_SHOW));
        jse.executeScript("window.scrollTo(0, 400);");
        SLIDE_SHOW.click();
        for (int i = 1; i < 39; i++) {
            wait.until(ExpectedConditions.urlToBe("http://www.directexpose.com/beauty-women-world/" + i + "/"));
            PageFactory.initElements(driver, this);
            if (i < 38) {
                wait.until(ExpectedConditions.visibilityOf(H2));
                jse.executeScript("window.scrollTo(0, 200);");
                wait.until(ExpectedConditions.visibilityOf(IMAGE));
                wait.until(ExpectedConditions.visibilityOf(NEXT_BTN_ON_IMG));
                //IMAGE.click();
                scrollToElementVertical("#next-button-tracking");
                wait.until(ExpectedConditions.visibilityOf(NEXT_BTN));
                NEXT_BTN.click();
            } else { //last page
                wait.until(ExpectedConditions.visibilityOf(LAST_PAGE_INDICATOR));
            }
        }
    }

    private void scrollToElementVertical(String selector) {

        try {

            JavascriptExecutor jse = (JavascriptExecutor) driver;
            jse.executeScript("var element = document.querySelector('" + selector + "');" +
                    "var rect = element.getBoundingClientRect();" +
                    "var absoluteElementTop = rect.top + window.pageYOffset;" +
                    "var middle = absoluteElementTop - (window.innerHeight / 2);" +
                    "window.scrollTo(0, middle);");
            sleep(500);
        } catch (Exception e) {
            Assert.fail();
        }

    }


    @AfterTest
    public void afterTest() {
        driver.close();
    }


}
